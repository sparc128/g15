//=========================================== Рисуем мышкой ==========================================================

// рисуем мышкой
function appDraw() {
// реконнект к canvas
    var canvas = document.getElementById("canvas_images");
    var context = canvas.getContext("2d");

    let w = canvas.width;
    let h = canvas.height;

    var mouse = {x: 0, y: 0};
    var draw = false;

    canvas.addEventListener("mousedown", function (e) {

        mouse.x = e.pageX - this.offsetLeft;
        mouse.y = e.pageY - this.offsetTop;
        draw = true;
        context.beginPath();
        context.moveTo(mouse.x, mouse.y);
    });
    canvas.addEventListener("mousemove", function (e) {

        if (draw == true) {
            mouse.x = e.pageX - this.offsetLeft;
            mouse.y = e.pageY - this.offsetTop;
            context.lineTo(mouse.x, mouse.y);
            context.stroke();
        }
    });
    canvas.addEventListener("mouseup", function (e) {

        mouse.x = e.pageX - this.offsetLeft;
        mouse.y = e.pageY - this.offsetTop;
        context.lineTo(mouse.x, mouse.y);
        context.stroke();
        context.closePath();
        draw = false;
    });
}
appDraw();

//=========================================== Строим ВЫКРОЙКУ ==========================================================

// построение по параметрам выкройки
function cVmode() {
    let poleRezNizLevo = document.getElementById("shirina").value;
    let poleRezNizPravo = document.getElementById("visota").value;
    let poleRezSeredinaLevo = document.getElementById("shirPlech").value;
    let poleRezSeredinaPravo = document.getElementById("shirGorlo").value;
    let poleRezVerhLevo = document.getElementById("visotaGorlo").value;
    let poleUgolSleva = document.getElementById("ugolSleva").value;
    let polePlechoLevoe = document.getElementById("plechoLevoe").value;
    let poleRezNiz = document.getElementById("nizShirina").value;


    var canvas = document.querySelector('canvas');
    if (canvas.getContext) {
        var ctx = canvas.getContext('2d');

        // функция очистки canvas по нажатию кнопки
        function ccc() {
            // по нажатию на кнопку Очистка Canvas - очищается Canvas
            var bc = document.getElementById("crect");
            bc.addEventListener("click", cll, false);

            function cll() {
                ctx.clearRect(0, 0, 900, 900);
            }
        }
        ccc();

        ctx.fillStyle = '#000000'; //для линий
        ctx.strokeStyle = 'rgba(255,255,255, 1)'; //для закрашенных фигур

        // переменные для сетки
        const canvasPlotWidth = canvas.clientWidth;
        const canvasPlotHeight = canvas.clientHeight; // ширина и высота canvas
        const scaleX = 25; // отступы между линиями
        const scaleY = 25;
        const xAxis = Math.round(canvasPlotWidth / scaleX / 2) * scaleX; // оси привязываются к сетке
        const yAxis = Math.round(canvasPlotHeight / scaleY / 2) * scaleY; // если мы меняем ее шаг
        const shiftNumberNames = 9; // отступы цифр осей
        const shiftAxisNames = 7;

        // переменные для чертежа
        let x = 0;
        let y = 0;
        let z1 = poleRezNizLevo; // длина отрезка 1
        let z2 = poleRezNizPravo; // длина отрезка 2
        let z3 = poleRezSeredinaLevo; // длина отрезка 3
        let z4 = poleRezSeredinaPravo; // длина отрезка 4
        let z5 = poleRezVerhLevo; // длина отрезка 5
        let z6 = poleUgolSleva; // длина отрезка 6
        let z7 = polePlechoLevoe; // длина отрезка 7
        let z8 = poleRezNiz; // длина отрезка 2 равняется длине отрезка 8


        // сетка
        ctx.beginPath();
        for (let i = 0; i <= canvasPlotWidth; i = i + scaleX){
            ctx.moveTo(i,0);
            ctx.lineTo(i, canvasPlotHeight);
            ctx.fillText((i - xAxis) / scaleX, i + (shiftNumberNames -5), yAxis + shiftNumberNames);
            ctx.strokeStyle = '#f5f0e8';
            ctx.lineWidth = 1;
        }
        let i = 0;
        do {
            i += scaleY;
            ctx.moveTo(0,i);
            ctx.lineTo(canvasPlotWidth, i);
            ctx.fillText((yAxis - i) / scaleY, xAxis + (shiftNumberNames - 5), i + shiftNumberNames);
            ctx.strokeStyle = '#f5f0e8';
            ctx.lineWidth = 1;
        } while ( i <= canvasPlotHeight );
        ctx.stroke();
        ctx.closePath();

        // оси
        ctx.beginPath();

        ctx.strokeStyle = '#ff0000';
        ctx.lineWidth = 1;

        ctx.moveTo(xAxis, 0); // верт ось
        ctx.lineTo(xAxis, canvasPlotHeight);
        ctx.fillText('y', xAxis - (shiftAxisNames + 3), 10); // обозначение оси Y

        ctx.moveTo(0, yAxis); // гор ось
        ctx.lineTo(canvasPlotWidth, yAxis);
        ctx.fillText('x', canvasPlotWidth - shiftAxisNames, yAxis - shiftAxisNames); // Обозначение оси X

        ctx.stroke();
        ctx.closePath();





        // начало движка
        let px1 = document.getElementById("px1");

        ctx.lineJoin = 'round'; // скругление краев угла
        ctx.lineCap = 'round';  // скругление краев линий
        ctx.lineWidth = 3;
        ctx.beginPath();


        x = 250;
        y = 100;

        // линия 1 - и преобразуем из градусов в радианы в переменной radians
        ctx.moveTo(x, y);
        ctx.strokeStyle = '#000000';
        let radians1 = 90 * Math.PI / 180;
        let x1 = 0;
        x1 = x + z1 * Math.cos(radians1);
        let y1 = 0;
        y1 = y + z1 * Math.sin(radians1);
        ctx.lineTo(x1, y1);


        // линия 2 - Высота юбки
        ctx.moveTo(x1, y1);
        let radians2 = -90 * Math.PI / 180;
        let x2 = 0;
        x2 = x1 + z2 * Math.cos(radians2);
        let y2 = 0;
        y2 = y1 + z2 * Math.sin(radians2);
        ctx.lineTo(x2, y2);
        // линия 2 - размеры
        ctx.fillText(z2, x2 + 15, y2 + 100);
        ctx.font = '14px PT sans';
        // Надпись - переднее полотнище
        ctx.fillText("З.П.", x2 - 75, y2 + 180);
        ctx.font = '14px PT sans';

        //ctx.arc(x1,y1,50, 0*Math.PI,1.5*Math.PI, true);


        /* ЗЕРКАЛЬНАЯ линия 2 - Высота юбки
           k - отступ между сторонами выкройки
        */
        let k=100;
        xx1 = x1+k;
        let yy1 = 0;
        yy1 = y1;
        ctx.moveTo(xx1, yy1);
        let radians22 = -90 * Math.PI / 180;
        let xx2 = 0;
        xx2 = xx1 + z2 * Math.cos(radians22);
        let yy2 = 0;
        yy2 = yy1 + z2 * Math.sin(radians22);
        ctx.lineTo(xx2, yy2);
        // Надпись - переднее полотнище
        ctx.fillText("П.П.", xx2 + 50, yy2 + 180);
        ctx.font = '14px PT sans';
        // Линия стрелка справа
        let xxx1 = 0;
        xx1 += 175;
        xxx1 = xx1;
        let yyy1 = 0;
        yy1 += 25;
        yyy1 = yy1;
        ctx.moveTo(xxx1, yyy1);
        let radians222 = -90 * Math.PI / 180;
        let xxx2 = 0;
        let zzz = 50;
        xxx2 = xxx1 + zzz * Math.cos(radians222);
        let yyy2 = 0;
        yyy2 = yyy1 + zzz * Math.sin(radians222);
        ctx.lineTo(xxx2, yyy2);
        // дописать стрелочки
        

        // линия 3 - верхний угол среза кромки юбки
        ctx.moveTo(x2, y2);
        let radians3 = 160 * Math.PI / 180;
        let x3 = 0;
        x3 = x2 + z3 * Math.cos(radians3);
        let y3 = 0;
        y3 = y2 + z3 * Math.sin(radians3);
        ctx.lineTo(x3, y3);
        // линия 3 - размеры
        ctx.fillText(z3, x3 + 15, y3 + -40);
        ctx.font = '12px PT sans';


        // ЗЕРКАЛЬНАЯ линия 3 - верхний угол среза кромки юбки
        ctx.moveTo(xx2, yy2);
        let radians33 = 20 * Math.PI / 180;
        let xx3 = 0;
        xx3 = xx2 + z3 * Math.cos(radians33);
        let yy3 = 0;
        yy3 = yy2 + z3 * Math.sin(radians33);
        ctx.lineTo(xx3, yy3);

        // линия 4
        ctx.moveTo(x3, y3);
        let radians4 = 120 * Math.PI / 180;
        let x4 = 0;
        x4 = x3 + z4 * Math.cos(radians4);
        let y4 = 0;
        y4 = y3 + z4 * Math.sin(radians4);
        ctx.lineTo(x4, y4);

        // ЗЕРКАЛЬНАЯ линия 4 - + исправить градус в радианах
        ctx.moveTo(xx3, yy3);
        let radians44 = 60 * Math.PI / 180;
        let xx4 = 0;
        xx4 = xx3 + z4 * Math.cos(radians44);
        let yy4 = 0;
        yy4 = yy3 + z4 * Math.sin(radians44);
        ctx.lineTo(xx4, yy4);




        // линия 6
        ctx.moveTo(x4, y4);
        let radians5 = -120 * Math.PI / 180; // преобразуем из градусов в радианы
        let x5 = 0;
        x5 = x4 + z5 * Math.cos(radians5);
        let y5 = 0;
        y5 = y4 + z5 * Math.sin(radians5);
        ctx.lineTo(x5, y5);

        // ЗЕРКАЛЬНАЯ линия 6 - исправить градус в радианах - ГОТОВО
        ctx.moveTo(xx4, yy4);
        let radians55 = 300 * Math.PI / 180;
        let xx5 = 0;
        xx5 = xx4 + z5 * Math.cos(radians55);
        let yy5 = 0;
        yy5 = yy4 + z5 * Math.sin(radians55);
        ctx.lineTo(xx5, yy5);

        // линия 7
        ctx.moveTo(x5, y5);
        let radians6 = 160 * Math.PI / 180; // преобразуем из градусов в радианы
        let x6 = 0;
        x6 = x5 + z6 * Math.cos(radians6);
        let y6 = 0;
        y6 = y5 + z6 * Math.sin(radians6);
        ctx.lineTo(x6, y6);
        // линия 7 - размеры
        ctx.fillText(z6, x6 + 15, y6 - 60);
        ctx.font = '12px PT sans';




        // ЗЕРКАЛЬНАЯ линия 7
        ctx.moveTo(xx5, yy5);
        let radians66 = 20 * Math.PI / 180; // преобразуем из градусов в радианы
        let xx6 = 0;
        xx6 = xx5 + z6 * Math.cos(radians66);
        let yy6 = 0;
        yy6 = yy5 + z6 * Math.sin(radians66);
        ctx.lineTo(xx6, yy6);

        // линия 8
        ctx.moveTo(x6, y6);
        var radians7 = 90 * Math.PI / 180;
        let x7 = 0;
        x7 = x6 + z7 * Math.cos(radians7);
        let y7 = 0;
        y7 = y6 + z7 * Math.sin(radians7);
        ctx.lineTo(x7, y7);
        // линия 8 - размеры
        ctx.fillText(z7, x7 - 35, y7 - 60);
        ctx.font = '12px PT sans';

        // ЗЕРКАЛЬНАЯ линия 8
        ctx.moveTo(xx6, yy6);
        var radians77 = 90 * Math.PI / 180;
        let xx7 = 0;
        xx7 = xx6 + z7 * Math.cos(radians77);
        let yy7 = 0;
        yy7 = yy6 + z7 * Math.sin(radians77);
        ctx.lineTo(xx7, yy7);

        // линия 9 - прямая - замыкает нижний левый конец линии с нижним правым концом линии
        ctx.moveTo(x7, y7);
        let radians8 = 0 * Math.PI / 180;
        let x8 = 0;
        x8 = x7 + z8 * Math.cos(radians8);
        let y8 = 0;
        y8 = y7 + z8 * Math.sin(radians8);
        ctx.lineTo(x8, y8);

        // ЗЕРКАЛЬНАЯ линия 9
        ctx.moveTo(xx7, yy7);
        let radians88 = 180 * Math.PI / 180;
        let xx8 = 0;
        xx8 = xx7 + z8 * Math.cos(radians88);
        let yy8 = 0;
        yy8 = yy7 + z8 * Math.sin(radians88);
        ctx.lineTo(xx8, yy8);

        // прямоугольник по введеным пользователем координатам - низ юбки
        ctx.rect(x7,y7,z8,z8);
        // линия - размеры
        x7 = Math.round(x7);
        y7 = Math.round(y7);
        ctx.fillText(z8, x7 + 61, y7 + 163); //
        ctx.font = '12px PT sans';
        // ЗЕРКАЛЬНЫЙ прямоугольник по введеным пользователем координатам - низ юбки
        ctx.rect(xx8,yy8,z8,z8);

        // шлицы юбки
        // срез шлица
        ctx.moveTo(x7, y7+25);
        let radians9 = 135 * Math.PI / 180;
        let x9 = 0;
        let s1 = 0;
        s1 = (z8 / 5.5); // шаг сетки делает привязку шлицов к сетке
        x9 = x7 + s1 * Math.cos(radians9);
        let y9 = 0;
        y9 = y7 + s1 * Math.sin(radians9);
        y9 += 25;
        ctx.lineTo(x9, y9);

        // вертикальная линия шлица
        ctx.moveTo(x9, y9);
        let radians10 = 90 * Math.PI / 180;
        let x10 = 0;
        let s2 = 0;
        s2 = ((s1 * 3) + 20); // шаг сетки делает привязку шлицов к сетке
        x10 = x9 + s2 * Math.cos(radians10);
        let y10 = 0;
        y10 = y9 + s2 * Math.sin(radians10);
        ctx.lineTo(x10, y10);

        // нижняя горизонтальная линия шлица юбки
        ctx.moveTo(x10, y10);
        let radians11 = 0 * Math.PI / 180;
        let x11 = 0;
        let s3 = 0;
        s3 = s1  ; // шаг сетки делает привязку шлицов к сетке
        x11 = x10 + s3 * Math.cos(radians11);
        let y11 = 0;
        y11 = y10 + s3 * Math.sin(radians11);
        ctx.lineTo(x11, y11);
        // линия - размеры
        s3 = Math.round(s3);
        x11 = Math.round(x11);
        y11 = Math.round(y11);
        ctx.fillText(s3, x11 - 21, y11 + 25);
        ctx.font = '12px PT sans';

        // вторая нижняя горизонтальная линия шлица юбки
        ctx.moveTo(x10, y10);
        let radians12 = -180 * Math.PI / 180;
        let x12 = 0;
        let s4 = 0;
        s4 = s1 - 7  ; // шаг сетки делает привязку шлицов к сетке
        x12 = x10 + s4 * Math.cos(radians12);
        let y12 = 0;
        y12 = y10 + s4 * Math.sin(radians12);
        ctx.lineTo(x12, y12);
        // линия - размеры
        s4 = Math.round(s4);
        ctx.fillText(s4, x12 + 1, y12 + 25);
        ctx.font = '12px PT sans';

        // вторая вертикальная линия шлица
        ctx.moveTo(x12, y12);
        let radians13 = -90 * Math.PI / 180;
        let x13 = 0;
        let s5 = 0;
        s5 = ((s1 * 3) + 20); // шаг сетки делает привязку шлицов к сетке
        x13 = x12 + s5 * Math.cos(radians13);
        let y13 = 0;
        y13 = y12 + s5 * Math.sin(radians13);
        ctx.lineTo(x13, y13);
        // линия - размеры
        s5 = Math.round(s5);
        ctx.fillText(s5, x13 - 35, y13 + 60);
        ctx.font = '12px PT sans';

        // верхняя горизонтальная линия шлица юбки
        ctx.moveTo(x13, y13);
        let radians14 = 0 * Math.PI / 180;
        let x14 = 0;
        let s6 = 0;
        s6 = s1 - 7  ; // шаг сетки делает привязку шлицов к сетке
        x14 = x13 + s6 * Math.cos(radians14);
        let y14 = 0;
        y14 = y13 + s6 * Math.sin(radians14);
        ctx.lineTo(x14, y14);

        // линия - верхняя левая кромка юбки
        ctx.moveTo(x6, y6);
        let radians15 = -90 * Math.PI / 180;
        let x15 = 0;
        let s7 = 0;
        s7 = s1   ; // шаг сетки делает привязку шлицов к сетке
        x15 = x6 + s7 * Math.cos(radians15);
        let y15 = 0;
        y15 = y6 + s7 * Math.sin(radians15);
        ctx.lineTo(x15, y15);


        // линия горизонтальная - верхняя левая кромка юбки
        ctx.moveTo(x15, y15);
        let radians16 = 350 * Math.PI / 180;
        let x16 = 0;
        let s8 = 0;
        s8 = s1 * 2   ; // шаг сетки делает привязку шлицов к сетке
        x16 = x15 + s8 * Math.cos(radians16);
        let y16 = 0;
        y16 = y15 + s8 * Math.sin(radians16);
        ctx.lineTo(x16, y16);

        // линия вертикальная справа - верхняя левая кромка юбки
        ctx.moveTo(x16, y16);
        let radians17 = 94 * Math.PI / 180;
        let x17 = 0;
        let s9 = 0;
        s9 = s1 - 10    ; // шаг сетки делает привязку шлицов к сетке
        x17 = x16 + s9 * Math.cos(radians17);
        let y17 = 0;
        y17 = y16 + s9 * Math.sin(radians17);
        ctx.lineTo(x17, y17);



        ctx.stroke();
        ctx.closePath();

        function btnClearCanvas() {
            document.querySelector(".btn-neu").addEventListener("click", (e) => {
                toggleClass("clicked", "btn-neu", e.target);
            });

            function toggleClass(htmlClass, targetClass, node) {
                if (node.classList.contains(targetClass)) {
                    node.classList.toggle(htmlClass);
                } else {
                    toggleClass(htmlClass, targetClass, node.parentNode);
                }
            }
        }
        btnClearCanvas();


    }
}

// функция построения лекала по нажатию на кнопке
function lekalo() {
    document.getElementById("postroenie").onclick = cVmode;
}

// вызов построения лекала по нажатию на кнопке
lekalo();

//============================================ Чистим ПОЛЯ ввода =======================================================

// функция очищает форму
function clear() {
    document.getElementById("shirina").value = ''
    document.getElementById("visota").value = '';
    document.getElementById("shirPlech").value = '';
    document.getElementById("shirGorlo").value = '';
    document.getElementById("visotaGorlo").value = '';
    document.getElementById("ugolSleva").value = '';
    document.getElementById("plechoLevoe").value = '';
}

// функция орабатывает нажатие кнопки Очистка и вызывает функцию очистки полей ввода clear()
function formClear() {
    document.getElementById("clear").onclick = clear;
}

// вызов функции очистки полей ввода данных
formClear();

//============================================ Скрываем ПОЛЯ ввода Выкройки 1 ==========================================


function hidePanelInput1() {
    document.getElementById("visibleinput")
        .addEventListener("click", function () {
            document.getElementById("shirina").hidden = true;
            document.getElementById("visota").hidden = true;
            document.getElementById("shirPlech").hidden = true;
            document.getElementById("shirGorlo").hidden = true;
            document.getElementById("visotaGorlo").hidden = true;
            document.getElementById("ugolSleva").hidden = true;
            document.getElementById("plechoLevoe").hidden = true;
            document.getElementById("nizShirina").hidden = true;
            document.getElementById("labelN1").hidden = true;
            document.getElementById("labelN2").hidden = true;
            document.getElementById("labelN3").hidden = true;
            document.getElementById("labelN4").hidden = true;
            document.getElementById("labelN5").hidden = true;
            document.getElementById("labelN6").hidden = true;
            document.getElementById("labelN7").hidden = true;
            document.getElementById("labelN8").hidden = true;
        }, false);
}

hidePanelInput1();

function hidePanelInput2() {
    document.getElementById("hiddeninput")
        .addEventListener("click", function () {
            document.getElementById("shirina").hidden = false;
            document.getElementById("visota").hidden = false;
            document.getElementById("shirPlech").hidden = false;
            document.getElementById("shirGorlo").hidden = false;
            document.getElementById("visotaGorlo").hidden = false;
            document.getElementById("ugolSleva").hidden = false;
            document.getElementById("plechoLevoe").hidden = false;
            document.getElementById("nizShirina").hidden = false;
            document.getElementById("labelN1").hidden = false;
            document.getElementById("labelN2").hidden = false;
            document.getElementById("labelN3").hidden = false;
            document.getElementById("labelN4").hidden = false;
            document.getElementById("labelN5").hidden = false;
            document.getElementById("labelN6").hidden = false;
            document.getElementById("labelN7").hidden = false;
            document.getElementById("labelN8").hidden = false;
        }, false);
}

hidePanelInput2();

// let foo =  document.getElementById("visibleinput").addEventListener("click", hidePanelInput1);
// switch (foo) {
//     case 1:
//         hidePanelInput1();
//         document.getElementById("visibleinput").removeEventListener("click", hidePanelInput1);
//
//         break;
//     case 2:
//         hidePanelInput2();
//         break;
//     default:
//         console.log('default');
// }

// let result = '';
// let i = 0;
//
// for( let i = 0; i < 5; i++) {
//     if ( i = 1) {
//         hidePanelInput1();
//     } else if (i = 2) {
//         hidePanelInput2();
//     }
// }



//============================================= Построение Выкройки 2 ==================================================




//============================================= Сохраняем работоспособный код в закомментированном виде ================

// function cVmode() {
//     let poleRezNizLevo = document.getElementById("shirina").value;
//     let poleRezNizPravo = document.getElementById("visota").value;
//     let poleRezSeredinaLevo = document.getElementById("shirPlech").value;
//     let poleRezSeredinaPravo = document.getElementById("shirGorlo").value;
//     let poleRezVerhLevo = document.getElementById("visotaGorlo").value;
//     let poleUgolSleva = document.getElementById("ugolSleva").value;
//     let polePlechoLevoe = document.getElementById("plechoLevoe").value;
//
//     var canvas = document.querySelector('canvas');
//     if (canvas.getContext) {
//         var ctx = canvas.getContext('2d');
//
//         ctx.fillStyle = '#000000'; //для линий
//         ctx.strokeStyle = 'rgba(255,255,255, 1)'; //для закрашенных фигур
//
//
//         let x = 0;
//         let y = 0;
//         let z1 = poleRezNizLevo; // длина отрезка 1
//         let z2 = poleRezNizPravo; // длина отрезка 2
//         let z3 = poleRezSeredinaLevo; // длина отрезка 3
//         let z4 = poleRezSeredinaPravo; // длина отрезка 4
//         let z5 = poleRezVerhLevo; // длина отрезка 5
//         let z6 = poleUgolSleva; // длина отрезка 6
//         let z7 = polePlechoLevoe; // длина отрезка 7
//         let z8 = poleRezNizPravo; // длина отрезка 2 равняется длине отрезка 8
//
//         ctx.lineJoin = 'round'; // скругление краев угла
//         ctx.lineCap = 'round';  // скругление краев линий
//         ctx.lineWidth = 5;
//         ctx.strokeStyle = '#000000';
//         ctx.beginPath();
//
//         // начало движка
//         x = 275;
//         y = 100;
//
//         // линия 1 - и преобразуем из градусов в радианы в переменной radians
//         ctx.moveTo(x, y);
//         let radians1 = 90 * Math.PI / 180;
//         let x1 = 0;
//         x1 = x + z1 * Math.cos(radians1);
//         let y1 = 0;
//         y1 = y + z1 * Math.sin(radians1);
//         ctx.lineTo(x1, y1);
//
//         // линия 2
//         ctx.moveTo(x1, y1);
//         let radians2 = -90 * Math.PI / 180;
//         let x2 = 0;
//         x2 = x1 + z2 * Math.cos(radians2);
//         let y2 = 0;
//         y2 = y1 + z2 * Math.sin(radians2);
//         ctx.lineTo(x2, y2);
//
//         // линия 3
//         ctx.moveTo(x2, y2);
//         let radians3 = 180 * Math.PI / 180;
//         let x3 = 0;
//         x3 = x2 + z3 * Math.cos(radians3);
//         let y3 = 0;
//         y3 = y2 + z3 * Math.sin(radians3);
//         ctx.lineTo(x3, y3);
//
//         // линия 4
//         ctx.moveTo(x3, y3);
//         let radians4 = 120 * Math.PI / 180;
//         let x4 = 0;
//         x4 = x3 + z4 * Math.cos(radians4);
//         let y4 = 0;
//         y4 = y3 + z4 * Math.sin(radians4);
//         ctx.lineTo(x4, y4);
//
//         // линия 5
//         ctx.moveTo(x4, y4);
//         let radians5 = 180 * Math.PI / 180; // преобразуем из градусов в радианы
//         let x5 = 0;
//         x5 = x4 + z5 * Math.cos(radians5);
//         let y5 = 0;
//         y5 = y4 + z5 * Math.sin(radians5);
//         ctx.lineTo(x5, y5);
//
//         // линия 6
//         ctx.moveTo(x5, y5);
//         let radians6 = -120 * Math.PI / 180; // преобразуем из градусов в радианы
//         let x6 = 0;
//         x6 = x5 + z6 * Math.cos(radians6);
//         let y6 = 0;
//         y6 = y5 + z6 * Math.sin(radians6);
//         ctx.lineTo(x6, y6);
//
//         // линия 7
//         ctx.moveTo(x6, y6);
//         let radians7 = 180 * Math.PI / 180; // преобразуем из градусов в радианы
//         let x7 = 0;
//         x7 = x6 + z7 * Math.cos(radians7);
//         let y7 = 0;
//         y7 = y6 + z7 * Math.sin(radians7);
//         ctx.lineTo(x7, y7);
//
//         // линия 8
//         ctx.moveTo(x7, y7);
//         var radians8 = 90 * Math.PI / 180;
//         let x8 = 0;
//         x8 = x7 + z8 * Math.cos(radians8);
//         let y8 = 0;
//         y8 = y7 + z8 * Math.sin(radians8);
//         ctx.lineTo(x8, y8);
//
//
//         // ctx.moveTo(x8, y8);
//         // let radians9 = 90 * Math.PI / 180; // преобразуем из градусов в радианы
//         // let x9 = 0;
//         // x9 = x8 + z1 * Math.cos(radians9);
//         // let y9 = 0;
//         // y9 = y8 + z1 * Math.sin(radians9);
//         // ctx.lineTo(x9, y9);
//
//         ctx.stroke();
//         ctx.closePath();
//     }
// }

//============================================= Trash ==================================================================

// // функция очищает рабочую область canvas
// function clearCanvasUser() {
//     // Store the current transformation matrix
//     context.save();
//
//     // Use the identity matrix while clearing the canvas
//     context.setTransform(1, 0, 0, 1, 0, 0);
//     context.clearRect(0, 0, canvas.width, canvas.height);
//
//     // Restore the transform
//     context.restore();
// }
//
// // функция обрабатывает нажатие на кнопку очистка холста и вызывает фунцию очистки холста
// function clearCanvas() {
//     document.getElementById("clearcanvas").onclick = clearCanvasUser;
// }
//
// clearCanvas();

//====================================================================================================
// function postroenie() {
//     document.getElementById("postroenie").onclick = cVmode;
//     cclearr();
// }
// postroenie();

// function clearCanvasUser() {
//     ctx.clearRect(0, 0, canvas.width, canvas.height);
// }

// function cclearr() {
//     document.getElementById("clearCanvas").onclick = clearCanvasUser;
// }

// проверяем на заполнение поля данных
// function empty_form() {
//     let poleRezNizLevo = document.getElementById("shirina").value;
//     let poleRezNizPravo = document.getElementById("visota").value;
//     let poleRezSeredinaLevo = document.getElementById("shirPlech").value;
//     let poleRezSeredinaPravo = document.getElementById("shirGorlo").value;
//     let poleRezVerhLevo = document.getElementById("visotaGorlo").value;
//
//     if(poleRezNizLevo == '') {
//         alert('Вы забыли ввести параметры.');
//         return false;
//     } else {
//         if(poleRezNizPravo == '') {
//             alert('Вы забыли ввести параметры.');
//             return false;
//         } else {
//             if(poleRezSeredinaLevo == '') {
//                 alert('Вы забыли ввести параметры.');
//                 return false;
//             } else {
//                 if(poleRezSeredinaPravo == '') {
//                     alert('Вы забыли ввести параметры.');
//                     return false;
//                 } else {
//                     if(poleRezVerhLevo == '') {
//                         alert('Вы забыли ввести параметры.');
//                         return false;
//                     }
//                 }
//                 return true;
//             }
//         }
//     }
// }

// appVM();
//
// // вызов главной функции проекта
// function appVM() {
//     document.getElementById("clearr").onclick = clearFormFunc;  // при клике на кнопке ОЧИСТКА вызываем функцию clearFormFunc
//     document.getElementById("modeling").onclick = someFunc;     // при клике на кнопке МОДЕЛИНГ происходит построение нашей модели
//
//     someFunc();
//     clearFormFunc();
// }
//
// //моделируем прямоугольник
// function someFunc() {
//     let poleRezNizLevo = document.getElementById("shirina").value;
//     let poleRezNizPravo = document.getElementById("visota").value;
//     let poleRezSeredinaLevo = document.getElementById("shirPlech").value;
//     let poleRezSeredinaPravo = document.getElementById("shirGorlo").value;
//     let poleRezVerhLevo = document.getElementById("visotaGorlo").value;
//
// //моделируем прямоугольник
//     var canvas = document.querySelector('canvas');
//     if (canvas.getContext) {
//         var ctx = canvas.getContext('2d');
//
//         ctx.fillStyle = '#000000'; //для линий
//         ctx.strokeStyle = 'rgba(255,255,255, 1)'; //для закрашенных фигур
//        // ctx.style.borderColor = '#ff0000';
//
//         let x = 0;
//         x = poleRezNizLevo;
//         let y = 0;
//         y = poleRezNizLevo * 2;
//
//         let x1 = 0;
//         x1 = poleRezNizPravo;
//         let y1 = 0;
//         y1 = poleRezNizPravo * 2;
//
//         let x2 = 0;
//         x2 = poleRezSeredinaLevo;
//         let y2 = 0;
//         y2 = poleRezSeredinaLevo * 2;
//
//         let x3 = 0;
//         x3 = poleRezSeredinaPravo;
//         let y3 = 0;
//         y3 = poleRezSeredinaPravo * 2;
//
//         let x4 = 0;
//         x4 = poleRezVerhLevo;
//         let y4 = 0;
//         y4 = poleRezVerhLevo * 2;
//
//         ctx.fillRect(0, 0, 400, 400); //прямоугольник закрашенный
//
//         ctx.lineJoin = 'round'; // скругление краев угла
//         ctx.lineCap = 'round';  // скругление краев линий
//         ctx.lineWidth = 5;
//         ctx.strokeStyle = '#ffff00';
//         ctx.beginPath();
//
//         ctx.moveTo(x, y);
//         // ctx.lineTo(x, y);
//         ctx.lineTo(x1, y1);
//
//         ctx.moveTo(x1, y1);
//         ctx.lineTo(x1 - x, y1 - y);
//
//         ctx.moveTo(x1, y1);
//         ctx.lineTo(x2, y2);
//
//         ctx.moveTo(x2, y2);
//         ctx.lineTo(x3, y3);
//
//         ctx.moveTo(x3, y3);
//         ctx.lineTo(x4, y4);
//
//         ctx.stroke();
//     }
// }






